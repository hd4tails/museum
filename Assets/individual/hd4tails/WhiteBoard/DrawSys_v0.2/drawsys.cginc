#ifndef DRAWSYS_CGINC
#define DRAWSYS_CGINC

#define dataMaxNum 4 //dataMaxNum*2の領域
#define data_penPos 0 //ペンのいち情報の色
#define data_penPos_b1 1 //ペンのいち情報の色(一つ前)
#define data_penEnable 2 //ペンのON/OFF情報の色(x:現在、y:1つ前）
#define data_penType 3 //ペンか消しゴムか(x:サイズ)
#define pos_controller_penType float2(0.5,1 - (6 - 0.5) / 6)
#define data_penSize 4 //ペンのサイズ(x:サイズ)
#define pos_controller_penSize float2(0.5,1 - (1 - 0.5) / 6)
#define data_penColor 5 //ペンの色
#define pos_controller_penColor_R float2(0.5,1 - (3 - 0.5) / 6)
#define pos_controller_penColor_G float2(0.5,1 - (4 - 0.5) / 6)
#define pos_controller_penColor_B float2(0.5,1 - (5 - 0.5) / 6)
#define data_eraserSize 6 //消しゴムのサイズ(x:サイズ)
#define pos_controller_eraserSize float2(0.5,1 - (2 - 0.5) / 6)
#define pos_center float2(0.5,0.5)
#define pen_size 0.05
#define pen_ThresholdONOFF 0.86
#define data_ON 1
#define data_OFF 0
#define posColorMagine 2

//0-1
float2 col2pos(float4 col,uint texWidthPix, uint texHeightPix)
{
	float2 pos = 0;
	
	pos.x = ( ( col.r +  int(fmod(col.b * 256,16 * posColorMagine) / posColorMagine ) ) ) * 256.0;
	pos.y = ( ( col.g +  int(col.b * 256 / 16 / posColorMagine) ) * 256.0 );
	if(texWidthPix <texHeightPix)
	{
		pos /= texHeightPix;
	}
	else
	{
		pos /= texWidthPix;
	}
	
	return pos;
}
//0-1
float2 fixAspect(float2 pos, uint texWidthPix, uint texHeightPix)
{
	float2 retpos = pos;
	if(texWidthPix < texHeightPix)
	{
		retpos.x *= (float)texWidthPix / texHeightPix;
	}
	else
	{
		retpos.y *= (float)texHeightPix / texWidthPix;
	}
	return retpos;
}
float4 pos2col(float2 pos, uint texWidthPix, uint texHeightPix)
{
	float xcolor = pos.x * texWidthPix;
	float ycolor = pos.y * texHeightPix;
	float rx = fmod(xcolor,256) / 255;
	float gy = fmod(ycolor,256) / 255;
	float bx = int(xcolor / 256) * posColorMagine;
	float by = int(ycolor / 256) * posColorMagine;
	float bxy = (bx + by * 16) / 255;
	return float4(rx,gy,bxy,1);
}
float calcDistance(float2 pos1,float2 pos2)
{
	return pow(pow(pos1.x - pos2.x,2) + pow(pos1.y - pos2.y,2), 0.5);
}
float2 index2memPos(uint index){
	float2 memPos;
	memPos.x = fmod(index,dataMaxNum);
	memPos.y = int(index / dataMaxNum);
	memPos /= dataMaxNum;
	return memPos;
}

uint memPos2index(float2 memPos){
	uint index = 0;
	
	index += int(memPos.x * dataMaxNum);
	index += int(memPos.y * dataMaxNum) * dataMaxNum;
	
	return index;
}
float cross2d(float2 vecA,float2 vecB)
{
    return vecA.x * vecB.y - vecA.y * vecB.x;
}

//線分ABからCまでの距離
float dist_line2point(float2 posA, float2 posB,float2 posC)
{
	//vector
	float2 vec_AB = posB - posA;
	float2 vec_AC = posC - posA;
	float2 vec_BC = posC - posB;
	
	//normalized vector
	float2 vec_norm_AB = normalize(vec_AB);
    
    //線分への垂線との交点をCとしたときのAC距離
    float dist_AC = dot(vec_norm_AB,vec_AC);
    
    //距離
    float dist = 1;
    if( dist_AC < 0 )
    {
        dist = length(vec_AC);
    }
    else if( dist_AC > length(vec_AB))
    {
        dist = length(vec_BC);
    }
    else
    {
        dist = abs(cross2d(vec_norm_AB,vec_AC));
    }
    return dist;
}

//return z 0は線分外、1は線分内
float3 nearpointonLine( float2 line1, float2 line2, float2 cpoint ){
	float2 a, b;
	float r;
	float3 nearpoint = 0;
	
	a.x = line2.x - line1.x;
	a.y = line2.y - line1.y;
	b.x = cpoint.x - line1.x;
	b.y = cpoint.y - line1.y;
	
	r = (a.x * b.x + a.y * b.y) / (a.x * a.x + a.y * a.y);
	
	if( r < 0 ){
		//nearpoint.xy = line1;
	}else if( r > 1 ){
		//nearpoint.xy = line2;
	}else{
		nearpoint.x = line1.x + r * a.x;
		nearpoint.y = line1.y + r * a.y;
		nearpoint.z = 1;
	}
	
	return nearpoint;
}
bool collisionLineCircle(float2 line1,float2 line2,float2 circle,float r)
{
	bool isin = false;
	float3 nearpoint = nearpointonLine(line1, line2, circle);
	
	if(nearpoint.z == 1)
	{
		if(calcDistance(nearpoint.xy,circle) <= r)
		{
			isin = true;
		}
	}
	
	return isin;
}

#endif /* DRAWSYS_CGINC */