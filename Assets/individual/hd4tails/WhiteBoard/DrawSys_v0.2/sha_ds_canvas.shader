﻿Shader "DrawSys_v0.2/ds_canvas"
{
    Properties
    {
		_MemoryTex ("ds_memory", 2D) = "white" {}
    }

    SubShader
    {
        Lighting Off
        Blend One Zero

        Pass
        {
            Name "Init"
            CGPROGRAM
            #include "UnityCustomRenderTexture.cginc"
		    #include "drawsys.cginc"
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment frag
            #pragma target 3.0
            
			sampler2D _MemoryTex;

            float4 frag(v2f_customrendertexture IN) : COLOR
            {
				float4 penEnable = tex2D(_MemoryTex, index2memPos(data_penEnable));
			    float4 col = tex2D(_SelfTexture2D,IN.globalTexcoord);
                float4 returncol = float4(0,0,0,0);
				float4 penColNow = tex2D(_MemoryTex, index2memPos(data_penPos));
				float4 penColBefore = tex2D(_MemoryTex, index2memPos(data_penPos_b1));
			    float2 penPosNow = col2pos(penColNow,_CustomRenderTextureWidth,_CustomRenderTextureHeight);
			    float2 penPosBefore = col2pos(penColBefore,_CustomRenderTextureWidth,_CustomRenderTextureHeight);
				fixed4 pencol = tex2D(_MemoryTex, index2memPos(data_penColor));
				float penSize = tex2D(_MemoryTex, index2memPos(data_penSize)).r;
				float eraserSize = tex2D(_MemoryTex, index2memPos(data_eraserSize)).r;
				float penType = tex2D(_MemoryTex, index2memPos(data_penType)).r;
			    penSize *= pen_size;
                penSize = penSize == 0 ? 0.000001:penSize;
			    eraserSize *= pen_size;
                eraserSize = eraserSize == 0 ? 0.000001:eraserSize;
                returncol.rgb = col.rgb;
                // 色をalphaに保持した最新の線をもとに書く
                if(penEnable.x == data_OFF && penEnable.y == data_ON)
                {
                    if(penType > 0.5)
    			    {
                        returncol.rgb = lerp(col.rgb,pencol.rgb,col.a);
    			    }
    			    else
    			    {
                        returncol.rgb = lerp(col.rgb,float3(1,1,1),col.a);
    			    }
                }
                //alphaに最新の線を描く
                if(penEnable.x != data_OFF || penEnable.y != data_OFF )
                {
                    if(penEnable.x == data_ON && penEnable.y == data_ON)
                    {
                        float alphavalue;
                        if(penType > 0.5)
        			    {
                            alphavalue = 1 - saturate(pow(dist_line2point(penPosBefore,penPosNow,fixAspect(IN.globalTexcoord,_CustomRenderTextureWidth,_CustomRenderTextureHeight)) /penSize,8));
        			    }
        			    else
        			    {
                            alphavalue = 1 - saturate(pow(dist_line2point(penPosBefore,penPosNow,fixAspect(IN.globalTexcoord,_CustomRenderTextureWidth,_CustomRenderTextureHeight))/ eraserSize,8));
        			    }
                        alphavalue = max(col.a,alphavalue);
                        returncol.a = alphavalue;
                    }
                    else
                    {
                        returncol.a = col.a;
                    }
                }
    			return returncol;
            }
            ENDCG
        }
    }
}
