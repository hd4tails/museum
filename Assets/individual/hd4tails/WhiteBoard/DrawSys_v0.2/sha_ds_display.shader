﻿Shader "DrawSys_v0.2/ds_display"
{
	Properties
	{
		_MemoryTex ("ds_memory", 2D) = "white" {}
		_CanvasTex ("ds_canvas", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
		    #include "drawsys.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MemoryTex;
			sampler2D _CanvasTex;
			float4 _MemoryTex_ST;
		    float4 _MemoryTex_TexelSize;
		    float4 _CanvasTex_TexelSize;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MemoryTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float4 penEnable = tex2D(_MemoryTex, index2memPos(data_penEnable));
				fixed4 texcol = tex2D(_MemoryTex, index2memPos(data_penPos));
				fixed4 pencol = tex2D(_MemoryTex, index2memPos(data_penColor));
				float penSize = tex2D(_MemoryTex, index2memPos(data_penSize)).r;
				float eraserSize = tex2D(_MemoryTex, index2memPos(data_eraserSize)).r;
				float penType = tex2D(_MemoryTex, index2memPos(data_penType)).r;
			    penSize *= pen_size;
			    eraserSize *= pen_size;
			    fixed4 col = tex2D(_CanvasTex, i.uv);

			    float2 pos = col2pos(texcol,_CanvasTex_TexelSize.z,_CanvasTex_TexelSize.w);
			    float2 aspectuv = fixAspect(i.uv,_CanvasTex_TexelSize.z,_CanvasTex_TexelSize.w);
			    float dist = calcDistance(aspectuv, pos);
			    if(penType > 0.5)
			    {
                    if(penEnable.y == data_ON)
    			    {
                        col.rgb = lerp(col.rgb,pencol.rgb,col.a);
    			    }
    			    if(texcol.a > 0 && dist < penSize && penSize * 0.8 < dist){
    			        col = pencol;
    			    }
			    }
			    else
			    {
                    if(penEnable.y == data_ON)
    			    {
                        col.rgb = lerp(col.rgb,float3(1,1,1),col.a);
    			    }
			        
    			    if(texcol.a > 0 && dist < eraserSize && eraserSize * 0.8 < dist){
    			        col = float4(0.1,0.1,0.1,1);
    			    }
			    }
				return col;
			}
			ENDCG
		}
	}
}
