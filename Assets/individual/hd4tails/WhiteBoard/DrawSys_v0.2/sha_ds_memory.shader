Shader "DrawSys_v0.2/ds_memory"
{
    Properties
    {
		_MemoryTex ("ds_memory", 2D) = "white" {}
		_CamDepthTex ("ds_penDepth", 2D) = "white" {}
		_CamPosColorTex ("ds_penPos", 2D) = "white" {}
		_ControllerTex ("ds_conDepth", 2D) = "white" {}
    }

    SubShader
    {
        Lighting Off
        Blend One Zero

        Pass
        {
            Name "Init"
            CGPROGRAM
            #include "UnityCustomRenderTexture.cginc"
		    #include "drawsys.cginc"
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment frag
            #pragma target 3.0
            
			sampler2D _MemoryTex;
			sampler2D _CamDepthTex;
			sampler2D _CamPosColorTex;
			sampler2D _ControllerTex;

            float4 frag(v2f_customrendertexture IN) : COLOR
            {
                float4 col = 0;
                switch(memPos2index(IN.globalTexcoord))
                {
                case data_penPos:
                    col = tex2D(_CamPosColorTex,pos_center);
                    break;
                case data_penPos_b1:
                    col = tex2D(_SelfTexture2D,index2memPos(data_penPos));
                    break;
                case data_penEnable:
                    float penDepth = tex2D(_CamDepthTex,pos_center).r;
                    col.y = tex2D(_SelfTexture2D,index2memPos(data_penEnable)).x;
                    if(penDepth > pen_ThresholdONOFF)
                    {
                        col.x = data_ON;
                    }
                    else
                    {
                        col.x = data_OFF;
                    }
                    break;
                case data_penType:
                    col.x = tex2D(_ControllerTex,pos_controller_penType).r < 0.5 ? 0 : 1;
                    break;
                case data_penSize:
                    col.x = tex2D(_ControllerTex,pos_controller_penSize).r;
                    break;
                case data_penColor:
                    col.r = tex2D(_ControllerTex,pos_controller_penColor_R).r;
                    col.g = tex2D(_ControllerTex,pos_controller_penColor_G).r;
                    col.b = tex2D(_ControllerTex,pos_controller_penColor_B).r;
                    col.a = 1;
                    break;
                case data_eraserSize:
                    col.x = tex2D(_ControllerTex,pos_controller_eraserSize).r;
                    break;
                default:
                    break;
                }
                return col;
            }
            ENDCG
        }
    }
}
