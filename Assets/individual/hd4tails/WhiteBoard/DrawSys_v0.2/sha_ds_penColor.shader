﻿Shader "DrawSys_v0.2/ds_penColor"
{
	Properties
	{
		_MemoryTex ("ds_memory", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
		    #include "drawsys.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MemoryTex;
			sampler2D _CanvasTex;
			float4 _MemoryTex_ST;
		    float4 _MemoryTex_TexelSize ;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MemoryTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 pencol = tex2D(_MemoryTex, index2memPos(data_penColor));
				return pencol;
			}
			ENDCG
		}
	}
}
