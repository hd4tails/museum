﻿Shader "DrawSys_v0.2/ds_posBase"
{
	Properties
	{
		
	}

	SubShader
	{
		Lighting Off
		Blend Off

		Pass
		{
			Name "Init"
			CGPROGRAM
			#include "UnityCustomRenderTexture.cginc"
			#include "drawsys.cginc"
			#pragma vertex CustomRenderTextureVertexShader
			#pragma fragment frag
			#pragma target 3.0
			

			float4 frag(v2f_customrendertexture IN) : COLOR
			{
				return pos2col(IN.globalTexcoord , _CustomRenderTextureWidth,_CustomRenderTextureHeight);
			}
			ENDCG
		}
	}
}
